

function ComponentField() {
    const columns: GridColumns<PokemonListItem> = [
        {
          field: "id",
          headerName: "Number",
          flex: 1,
        },
        {
          field: "name",
          headerName: "Pokemon",
          flex: 2,
        },
        {
          field: "img",
          headerName: "Image",
          flex: 2,
          renderCell: (params) => <img src={params.value}></img>,
        },
      ];
    
}

export ComponentField

