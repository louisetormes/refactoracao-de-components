import { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import { DataGrid, GridColumns } from "@mui/x-data-grid";
import { fetchPokemon, fetchPokemons } from "./services/fetch-pokemons";
import { Pokemon, PokemonListItem } from "./types";
import { apiPokemonToPokemonListItem } from "./mappers";
import {
  Autocomplete,
  Card,
  CardContent,
  CardMedia,
  List,
  ListItem,
  ListItemText,
  TextField,
  Typography,
} from "@mui/material";
import { Container, SxProps, width } from "@mui/system";
import { ComponentField } from "./components/ComponentField"
import { ComponentApp } from "./components/ComponentApp"

<ComponentField/>

<ComponentApp/>

export default App;
